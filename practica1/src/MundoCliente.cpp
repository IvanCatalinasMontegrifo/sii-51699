// MundoCliente.cpp: implementation of the CMundoCliente class.
// Iván Catalinas Montegrifo 51699
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>

#include <signal.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
struct sigaction act;
void controlador(int n){
	
	switch(n){
	case SIGUSR1:{
		printf("\nTermina correctamente\n");
		exit(1);
		break;
		} 
	case SIGINT: printf("\nsignal %d\n",n);exit(n); break;
	case SIGPIPE: printf("\nsignal %d\n",n);exit(n); break;
	case SIGTERM: printf("\nsignal %d\n",n);exit(n); break;
	case SIGALRM: printf("\nsignal %d\n",n);exit(n); break;
	default: break;	
	}
}

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	char cadenaFin[17];
	sprintf(cadenaFin, "Fin del juego. \n");
	//write(fd,cadenaFin,strlen(cadenaFin)+1);
	//close(fd);
	pDatos->accion=3;
	munmap(org,sizeof(datos));
	unlink("/tmp/DatosMemCompartida");

	close(fd_coordenadas);
	unlink("/tmp/fifo_coordenadas");
	
	write(fd_teclas,cadenaFin,strlen(cadenaFin)+1);
	close(fd_teclas);
	unlink("/tmp/fifo_teclas");
	

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	//int i;
	for(int i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	//esfera.Dibuja();
	for(int i=0;i<esferas.size();i++)
		esferas[i]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	//tiempo+=0.025f;

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	//esfera.Mueve(0.025f);
	//int i;

	for(int i=0;i<esferas.size();i++)
	{
		esferas[i]->Mueve(0.025f);
	}

	for(int i=0;i<paredes.size();i++)
	{
		//paredes[i].Rebota(esfera);
		for(int j=0;j<esferas.size();j++)
		{
			paredes[i].Rebota(*esferas[j]);
		}

		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	/*	
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
	*/
	for(int i=0;i<esferas.size();i++)
	{
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);
		if(fondo_izq.Rebota(*esferas[i]))
		{
			esferas.erase(esferas.begin()+i);
			if(esferas.size()==0)
			{
				Esfera *e = new Esfera;
				e->centro.x=0;
				e->centro.y=rand()/(float)RAND_MAX;
				e->radio=0.5f;
				e->velocidad.x=2+2*rand()/(float)RAND_MAX;
				e->velocidad.y=2+2*rand()/(float)RAND_MAX;
				esferas.push_back(e);	
			}
			puntos2++;
			//char cadena1[60];
			//sprintf(cadena1,"Jugador 2 marca 1 punto, lleva un total de %d puntos. \n", puntos2);
			//write(fd,cadena1,strlen(cadena1)+1);
		}
		if(fondo_dcho.Rebota(*esferas[i]))
		{
			esferas.erase(esferas.begin()+i);
			if(esferas.size()==0)
			{
				Esfera *e = new Esfera;
				e->centro.x=0;
				e->centro.y=rand()/(float)RAND_MAX;
				e->radio=0.5f;
				e->velocidad.x=2+2*rand()/(float)RAND_MAX;
				e->velocidad.y=2+2*rand()/(float)RAND_MAX;
				esferas.push_back(e);	
			}
			puntos1++;
			//char cadena2[60];
			//sprintf(cadena2,"Jugador 1 marca 1 punto, lleva un total de %d puntos. \n", puntos1);
			//write(fd,cadena2,strlen(cadena2)+1);
		}
	}
	
	/*
	if(tiempo>5.0f && esferas.size()<2)
	{
		tiempo=0.0f;
		Esfera *e = new Esfera;
		e->centro.x=0;
		e->centro.y=rand()/(float)RAND_MAX;
		e->radio=0.5f;
		e->velocidad.x=-2-2*rand()/(float)RAND_MAX;
		e->velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
	}
	*/

	pDatos->esfera.centro=esferas[0]->centro;
	pDatos->raqueta1=jugador1;	

	switch (pDatos->accion){
		case 1:
			OnKeyboardDown('w', 0, 0);
			break;
		case -1:
			OnKeyboardDown('s', 0, 0);
			break;
		case 0:
		    	jugador1.velocidad.y=0;
		    	break;
		default:
		   	break;

   	 }
	
	//Leer los datos de la tubería (Práctica 4)
	int read_error=read(fd_coordenadas,cadena,sizeof(cadena));
	if(read_error==-1){
		perror("Error al leer la tubería\n");
		exit(1);
	}	
	if(cadena[0]=='F')exit(1);
	//Actualizar los atributos de MundoCliente con los datos recibidos
	sscanf(cadena,"%f %f %f %f %f %f %f %f %f %f %d %d", &esferas[0]->centro.x,&esferas[0]->centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
	
	//if(puntos1==3 || puntos2==3)
		//exit(1);

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	
	//Escribe los datos de la tubería
	sprintf(cad_teclas,"%c",key);
	write(fd_teclas,cad_teclas,sizeof(cad_teclas));
}

void CMundoCliente::Init()
{
	act.sa_handler=controlador;
	act.sa_flags=0;
	sigaction(SIGALRM,&act,NULL);
	sigaction(SIGTERM,&act,NULL);
	sigaction(SIGPIPE,&act,NULL);
	sigaction(SIGINT,&act,NULL);
	sigaction(SIGUSR1,&act,NULL);
	
	esferas.push_back(new Esfera);
	//tiempo=0.0f;

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	/*
	fd=open("/tmp/fifo",O_WRONLY);
	if(fd==-1){
		perror("Error al abrir la tubería. \n");
		exit(1);
	}
	*/
	fd_datos=open("/tmp/DatosMemCompartida",O_RDWR|O_CREAT|O_TRUNC,0777);
	if(fd_datos==-1){
		printf("Error al abrir el fichero compartido. \n");
		close(fd_datos);
		exit(-1);
	}
	//Definir el tamaño del fichero
	write(fd_datos,&datos,sizeof(datos));
	    
	//Se proyecta todo el fichero origen
	org=(char*)mmap(NULL,sizeof(datos),PROT_WRITE|PROT_READ,MAP_SHARED,fd_datos,0); //Devuelve la dirección de proyección utlizada.
	if(org==MAP_FAILED){
		perror("No puede abrirse el fichero de proyección. \n");
		exit(-1);
	}
	//Cerrar el descriptor de fichero.
	close(fd_datos);
	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero creado en el paso 2.
	pDatos=(DatosMemCompartida*)org;
	pDatos->accion=0;


	//Creación de FIFO para coordenadas
	unlink("/tmp/fifo_coordenadas"); 
	int error_coordenadas= mkfifo("/tmp/fifo_coordenadas",0777);
	if(error_coordenadas){ 
		perror("Error al crear el FIFO de las coordenadas\n");
		exit(1);
	}

	//Creación de FIFO para teclas

	unlink("/tmp/fifo_teclas"); 

	int error_teclas= mkfifo("/tmp/fifo_teclas",0777);
	if(error_teclas==-1){ 
		perror("Error al crear el FIFO de las teclas\n");
		exit(1);
	}

	//Apertura de FIFO para coordenadas
	fd_coordenadas=open("/tmp/fifo_coordenadas",O_RDONLY);
	if(fd_coordenadas==-1){
		perror("Error al abrir el FIFO de las coordenadas\n");
		exit(1);
	}
	else
		printf("Correcta apertura del FIFO de las coordenadas\n");

	
	//Apertura de FIFO para teclas
	fd_teclas=open("/tmp/fifo_teclas",O_WRONLY);
	if(fd_teclas==-1){
		perror("Error al abrir el FIFO de las teclas\n");
		exit(1);
	}
	else
		printf("Correcta apertura del FIFO de las teclas\n");	
	
}
