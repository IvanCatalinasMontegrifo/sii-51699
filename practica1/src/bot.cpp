#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>

int main(){
	int fd;
	char *org;
	DatosMemCompartida *pDatos;
	float mitadRaqueta;

	fd=open("/tmp/DatosMemCompartida",O_RDWR);	
	if(fd==-1){
		perror("No puede abrirse el fichero compartido. \n");
		exit(1);
	}
	org=(char*)mmap(NULL, sizeof(*(pDatos)), PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);
	if(org==MAP_FAILED)
	{
		perror("Error en la proyeccion del fichero. \n");
		close(fd);
		exit(1);
	}
	pDatos=(DatosMemCompartida*)org;
	close(fd);
	while(1){
		if(pDatos->accion==3)break;
		mitadRaqueta=((pDatos->raqueta1.y1+ pDatos->raqueta1.y2)/2);
		if(mitadRaqueta < (pDatos->esfera.centro.y))
			pDatos->accion=1; //Arriba
		else if(mitadRaqueta > (pDatos->esfera.centro.y))
			pDatos->accion=-1; //Abajo		
		else pDatos->accion=0; //Nada
		usleep(25000);	
	}
	munmap(org,sizeof(*(pDatos)));
	unlink("/tmp/DatosMemCompartida");
	return 0;
}

