#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 60

int main()
{
	int fd;
	int create_error, read_error;
	//char *fifo="/tmp/fifo";
	char buffer[MAX];
	unlink("/tmp/fifo");
	create_error=mkfifo("/tmp/fifo",0777);
	if(create_error!=0){
		perror("mkfifo");
		exit(1);
	}
	fd=open("/tmp/fifo",O_RDONLY);
	if(fd==-1){
		perror("open");
		exit(1);
	}
	while(1){
		read_error=read(fd,buffer,MAX);
		if(read_error==-1){
			perror("read");
			exit(1);
		}
		else if(buffer[0]=='F'){
			printf("%s",buffer);
			break;
		}
		else{
			printf("%s \n", buffer);
		}
	}
	close(fd);
	unlink("/tmp/fifo");
	return 0;
}

